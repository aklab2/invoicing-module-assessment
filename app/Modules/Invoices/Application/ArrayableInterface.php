<?php 
namespace App\Modules\Invoices\Application;

interface ArrayableInterface{
    public function toArray();
}