<?php
namespace App\Modules\Invoices\Application;

use App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate;
use Ramsey\Uuid\UuidInterface;
interface InvoicesRepositoryInterface{


    public function getById(UuidInterface $id):InvoiceAggregate;
    public function setStatus(UuidInterface $uuid, \App\Domain\Enums\StatusEnum $status): bool;

}