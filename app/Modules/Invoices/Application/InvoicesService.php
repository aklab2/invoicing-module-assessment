<?php
namespace App\Modules\Invoices\Application;

use \App\Modules\Invoices\Api\Dto\InvoiceDto;
use App\Modules\Invoices\Api\InvoicesFacadeInterface;
use App\Modules\Invoices\Domain\Entities\InvoiceAggregate;
use App\Modules\Invoices\Application\InvoicesRepositoryInterface;
use LogicException;

class InvoicesService implements InvoicesFacadeInterface{

    public function __construct(
        private InvoicesRepositoryInterface $invoice_repository
        
    ) {
        //TODO:: must be DI
        //$this->invoice_repository = app()->make( InvoicesRepositoryInterface::class); 
    }


    public function getInvoice($id):InvoiceDto{
        //
        $uuid = \Ramsey\Uuid\Uuid::fromString($id);
        try{
            $invoice_aggregate = $this->invoice_repository->getById($uuid);
        }catch(InvoicesRepositoryException $e){
            throw new LogicException($e->getMessage());
        }
        

        //some logic - getting taxes, discounts and other additional information ..

        $default_company = new \App\Modules\Invoices\Domain\Entities\Company(null, name:"Our Company Inc", street:"647 Metropolitan way", city:"Des Plaines", zip:"60016",  phone: "+17730000000", email:null );
        
        //sales tax rate usually based on customer's location
        $default_sales_tax = new \App\Modules\Invoices\Domain\ValueObjects\SalesTax(0 );

        $invoice_aggregate->applySalesTax($default_sales_tax);
        $invoice_aggregate->setCompany($default_company);
        


        return InvoiceDto::fromEntity($invoice_aggregate);
    }


    public function approveInvoice($id):void{
        //

        $apapter = app()->make(\App\Modules\Invoices\Infrastructure\Adapters\ApprovalAdapter::class);
        
        $uuid = \Ramsey\Uuid\Uuid::fromString($id);
        try{
            $invoice_aggregate = $this->invoice_repository->getById($uuid);
        }catch(InvoicesRepositoryException $e){
            throw new LogicException($e->getMessage());
        }

        $apapter->approve($invoice_aggregate );
    
    }
    public function rejectInvoice($id):void{
        //

        $apapter = app()->make(\App\Modules\Invoices\Infrastructure\Adapters\ApprovalAdapter::class);
        $uuid = \Ramsey\Uuid\Uuid::fromString($id);
        try{
            $invoice_aggregate = $this->invoice_repository->getById($uuid);
        }catch(InvoicesRepositoryException $e){
            throw new LogicException($e->getMessage());
        }

        $apapter->reject($invoice_aggregate );
    
    }

    public function setStatus($uuid, $status){
        //
        $invoices_repository = new \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository();
        $entity = $invoices_repository->setStatus($uuid, $status );

    }

}