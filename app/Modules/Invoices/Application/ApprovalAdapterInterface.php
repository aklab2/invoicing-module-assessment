<?php

namespace App\Modules\Invoices\Application;


interface ApprovalAdapterInterface{

    public function approve(\App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate $invoice):void;
    public function reject(\App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate $invoice):void;

}