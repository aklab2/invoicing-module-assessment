<?php

namespace App\Modules\Invoices\Infrastructure\Database\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceProductLine extends Model{

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');

    }

}