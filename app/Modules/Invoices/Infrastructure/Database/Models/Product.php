<?php

namespace App\Modules\Invoices\Infrastructure\Database\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

}