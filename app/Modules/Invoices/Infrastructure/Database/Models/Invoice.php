<?php

namespace App\Modules\Invoices\Infrastructure\Database\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model{
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
        'status'

    ];
    public function lines()
    {
        return $this->hasMany(InvoiceProductLine::class, 'invoice_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

}