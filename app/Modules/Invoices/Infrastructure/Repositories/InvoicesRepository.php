<?php
namespace App\Modules\Invoices\Infrastructure\Repositories;

use App\Modules\Invoices\Application\InvoicesRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate;

class InvoicesRepository implements InvoicesRepositoryInterface{

    public function getById(UuidInterface $uuid): InvoiceAggregate
    {

     
        $model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::with('lines.product')->with('company')->find($uuid->toString());
        if(!$model){
            throw new \App\Modules\Invoices\Application\InvoicesRepositoryException("Entity not found");
        }


            //mapping EloquentModel -> Entity

   
            
            $billed_company = new \App\Modules\Invoices\Domain\Entities\Company(
                $model->company->id, 

                name:$model->company->name, 
                street:$model->company->street, 
                city:$model->company->city, 
                zip:$model->company->zip, 
       
                phone:$model->company->phone, 
                email:$model->company->email, 
            );
            $lines = $model->lines->map(function($line){
                if($line->product){
                    $product =  new \App\Modules\Invoices\Domain\Entities\Product($line->product->id,  name: $line->product->name, price:new \App\Modules\Invoices\Domain\ValueObjects\Money($line->product->price, $line->product->currency) );
                    return new \App\Modules\Invoices\Domain\Entities\InvoiceProductLine($line->id,  $line->quantity, $product );
                }
            });

      

            return new \App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate(
                id:$model->id,
                status: $model->status,
                number: $model->number,
                date: new \DateTime($model->date, new \DateTimeZone('UTC')),
                due_date: new \DateTime($model->due_date, new \DateTimeZone('UTC')),
                company: null,
                billed_company: $billed_company,
                lines: $lines->toArray(),
                sales_tax:null,


            );


    }

    public function setStatus(UuidInterface $uuid, \App\Domain\Enums\StatusEnum $status): bool
    {
        
        $model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::find($uuid->toString());
        
        if(!$model){
            throw new \App\Modules\Invoices\Application\InvoicesRepositoryException("Entity not found");
        }
        $model->update(['status'=>$status->value]);


        return true;
    }    

}