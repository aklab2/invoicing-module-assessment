<?php

namespace App\Modules\Invoices\Infrastructure\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use \App\Modules\Invoices\Application\InvoicesRepositoryInterface;
use LogicException;

class InvoicesController extends BaseController
{
    //
    public function test(Request $request){

        return $request->input();
    }

    public function show(Request $request, $id){
        
        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);
        //TODO:: $id validation
        try{
            $dto = $service->getInvoice($id);
            return response($dto->entity, 200);

        }catch(LogicException $e){
            //TODO:: move to LogicException handler
            return response(['message'=>$e->getMessage()], 422);
        }


    }
    public function approve(Request $request, $id){
        
        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);
        //TODO:: $id validation
        try{

            $service->approveInvoice($id);
            return response(['id'=>$id], 200);

        }catch(LogicException $e){
            //TODO:: move to LogicException handler
            return response(['message'=>$e->getMessage()], 422);
        }
    }
    public function reject(Request $request, $id){
        
        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);
        //TODO:: $id validation
        try{
            
            $service->rejectInvoice($id);
            return response(['id'=>$id], 200);

        }catch(LogicException $e){
            //TODO:: move to LogicException handler
            return response(['message'=>$e->getMessage()], 422);
        }


    }    



}
