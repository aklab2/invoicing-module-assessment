<?php

namespace App\Modules\Invoices\Infrastructure\Adapters;
use Illuminate\Events\Dispatcher;

use App\Modules\Invoices\Application\ApprovalAdapterInterface;

class ApprovalAdapter implements ApprovalAdapterInterface{
    

    public function __construct(
        private \App\Modules\Approval\Api\ApprovalFacadeInterface $approval_api 
    ){

    }

    public function approve(\App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate $invoice):void{
        
        $uuid = \Ramsey\Uuid\Uuid::fromString($invoice->id);
        $dto = new \App\Modules\Approval\Api\Dto\ApprovalDto($uuid, $invoice->status, $invoice->toJson());
        $this->approval_api->approve( $dto );
    }
    public function reject(\App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate $invoice):void{
        $uuid = \Ramsey\Uuid\Uuid::fromString($invoice->id);
        $dto = new \App\Modules\Approval\Api\Dto\ApprovalDto($uuid, $invoice->status, $invoice->toJson());
        $this->approval_api->reject( $dto );
    }    

    public  function getApi(){

        return $this->approval_api;
    }


    public function subscribe(Dispatcher $events): void
    {


        $events->listen(
            [\App\Modules\Approval\Api\Events\EntityApproved::class, \App\Modules\Approval\Api\Events\EntityRejected::class],
            function($event){
            
                $service = app()->make(\App\Modules\Invoices\Application\InvoicesService::class);
                $status = ($event instanceof \App\Modules\Approval\Api\Events\EntityApproved)?\App\Domain\Enums\StatusEnum::APPROVED :\App\Domain\Enums\StatusEnum::REJECTED;

                $service->setStatus($event->approvalDto->id,$status);
            }
        );

    }


}
