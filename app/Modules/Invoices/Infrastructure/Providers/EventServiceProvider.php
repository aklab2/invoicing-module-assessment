<?php
 
namespace App\Providers;
 
namespace App\Modules\Invoices\Infrastructure\Providers;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
 
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // ...
    ];
 
    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        \App\Modules\Invoices\Infrastructure\Adapters\ApprovalAdapter::class
    ];
}