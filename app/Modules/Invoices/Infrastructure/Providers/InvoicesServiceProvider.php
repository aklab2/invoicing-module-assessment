<?php

namespace App\Modules\Invoices\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;

class InvoicesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->scoped(\App\Modules\Invoices\Application\InvoicesRepositoryInterface::class, \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository::class);        

        $this->app->scoped(\App\Modules\Invoices\Application\ApprovalAdapterInterface::class, \App\Modules\Invoices\Infrastructure\Adapters\ApprovalAdapter::class);  
        $this->app->scoped(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class, \App\Modules\Invoices\Application\InvoicesService::class); 
        

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //

    }
}
