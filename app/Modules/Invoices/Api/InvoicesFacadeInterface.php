<?php

namespace App\Modules\Invoices\Api;

use App\Modules\Invoices\Api\Dto\InvoiceDto;

interface InvoicesFacadeInterface{

    public function getInvoice(string $id):InvoiceDto;
    public function approveInvoice(string $id):void;
    public function rejectInvoice(string $id):void;

}