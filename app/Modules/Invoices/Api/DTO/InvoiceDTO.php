<?php

namespace App\Modules\Invoices\Api\Dto;


use App\Domain\Enums\StatusEnum;
use Ramsey\Uuid\UuidInterface;

final readonly class InvoiceDto
{
    /** @param class-string $entity */
    public function __construct(
        public UuidInterface $id,
        public StatusEnum $status,
        public string $entity,
    ) {
    }


    static public function fromEntity(\App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate $invoice){


        $uuid = \Ramsey\Uuid\Uuid::fromString($invoice->id);
        return new self($uuid, $invoice->status, $invoice->toJson());

    }
}