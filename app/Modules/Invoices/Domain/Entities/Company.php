<?php


namespace App\Modules\Invoices\Domain\Entities;
use \App\Modules\Invoices\Domain\GetValueTrait;
use App\Modules\Invoices\Application\ArrayableInterface;

class Company {
    use GetValueTrait;
    public function __construct(
        private ?string $id = null,
        private string $name,
        private string $street,
        private string $city,
        //private string $state,
        private string $zip,
        private string $phone,
        private ?string $email,
    ){


    }
    public function toArray(){
        return [

            'id'=>$this->id,
            'name'=>$this->name,

            'street'=>$this->street,
            'city'=>$this->city,
            'zip'=>$this->zip,
            'phone'=>$this->phone,
            'email'=>$this->email,
            //TODO
        ];
    }

}