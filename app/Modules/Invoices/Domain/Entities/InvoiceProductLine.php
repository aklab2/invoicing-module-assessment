<?php

namespace App\Modules\Invoices\Domain\Entities;

use App\Modules\Invoices\Domain\GetValueTrait;

class InvoiceProductLine{


    use GetValueTrait;



    public function __construct(

        private ?string $id = null,
        private int $quantity = 0,
        private Product $product,
    )
    {
        
    }

    public function toArray(){

        return [

            'id'=>$this->id,
            'product'=>$this->product?->name,
            'quantity'=>$this->quantity,
            'unit_price'=>$this->product?->price->toArray(),

            'total'=>$this->calculateLineTotal()->toArray(),

            

        ];
    }
    private function calculateLineTotal(){

        return new \App\Modules\Invoices\Domain\ValueObjects\Money($this->quantity*$this->product->price->amount??0, $this->product->price->currency);
    }

    
}