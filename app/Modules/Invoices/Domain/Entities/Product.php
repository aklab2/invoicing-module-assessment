<?php


namespace App\Modules\Invoices\Domain\Entities;

use \App\Modules\Invoices\Domain\GetValueTrait;
use App\Modules\Invoices\Domain\ValueObjects\Money;
use App\Modules\Invoices\Application\ArrayableInterface;

class Product{
    use GetValueTrait;
    public function __construct(
        private ?string $id = null,
        private string $name,
        private Money $price = new Money(0, 'usd'),
    ){


    }
    public function toArray(){
        return [

            'id'=>$this->id,
            'name'=>$this->name,
            'product'=>$this->price?->toArray()
        ];
    }
    public function __toString()
    {
        return "{$this->name} {$this->price}";
    }

}