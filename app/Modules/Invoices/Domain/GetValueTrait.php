<?php
namespace App\Modules\Invoices\Domain;
//\App\Modules\Invoices\Domain\GetValueTrait

trait GetValueTrait{
    
    public function __get($key){
    
        return $this->$key??null;

    }

}