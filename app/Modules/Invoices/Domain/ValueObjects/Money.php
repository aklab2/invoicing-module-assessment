<?php

namespace App\Modules\Invoices\Domain\ValueObjects;

use App\Modules\Invoices\Domain\GetValueTrait;
use App\Modules\Invoices\Application\ArrayableInterface;

class Money{
    use GetValueTrait;

    public function __construct(
        private int $amount = 0, // cents
        private string $currency = 'usd', //according to task USD is a single currency
    )
    {

        
    }
    public function toArray(){
        return [


            'amount'=>$this->amount,
            'currency'=>$this->currency,
            'display_amount'=>(string)$this
        ];
    }
    public function __toString()
    {
        $fmt = new \NumberFormatter( 'en_US', \NumberFormatter::CURRENCY );
        return  $fmt->formatCurrency($this->amount/100, $this->currency);

    }
}