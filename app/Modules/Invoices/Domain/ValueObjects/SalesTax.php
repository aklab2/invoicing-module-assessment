<?php


namespace App\Modules\Invoices\Domain\ValueObjects;

use App\Modules\Invoices\Domain\GetValueTrait;

class SalesTax{
    use GetValueTrait;

    public function __construct(
        private float $rate = 0
    ){

    }
    public function __toString()
    {
        return "{$this->rate}%";
    }

}