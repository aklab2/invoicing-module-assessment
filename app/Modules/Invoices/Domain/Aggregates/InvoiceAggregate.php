<?php


namespace App\Modules\Invoices\Domain\Aggregates;

use DateTime;
use App\Domain\Enums\StatusEnum;

use App\Modules\Invoices\Domain\Entities\Company;
use App\Modules\Invoices\Domain\Entities\InvoiceProductLine;
use App\Modules\Invoices\Domain\GetValueTrait;
use App\Modules\Invoices\Domain\ValueObjects\SalesTax;
use App\Modules\Invoices\Domain\ValueObjects\Money;

use App\Modules\Invoices\Application\ArrayableInterface;

class InvoiceAggregate implements ArrayableInterface{
    
    use GetValueTrait;

    private Money $subtotal;
    private Money $tax;
    private Money $total;

    private StatusEnum $status;

    public function __construct(
    private ?string $id = null,
    string $status,
    private string $number,
    private DateTime $date,
    private DateTime $due_date,

    private ?Company $company,

    private ?Company $billed_company,
    private array $lines = [],
    private ?SalesTax $sales_tax,
    

    )
    {

        $this->status = StatusEnum::tryFrom($status);
    }

    public function addInvoiceProductLine(InvoiceProductLine $lines){


        //TODO

    }
    public function setCompany(\App\Modules\Invoices\Domain\Entities\Company $company){

        $this->company = $company;//      

    }

    public function  applySalesTax(\App\Modules\Invoices\Domain\ValueObjects\SalesTax $tax){

        $this->sales_tax = $tax; 

    }
    public function calculateSubTotal():Money{

        $subtotal = 0;

        foreach($this->lines as $line){
            
            $subtotal += $line->product->price->amount * $line->quantity;
        }
        return new Money($subtotal, 'usd');

    }

    public function calculateTax():Money{
        if($this->sales_tax){
            $subtotal = $this->calculateSubTotal();
            return new Money(round($subtotal->amount * $this->sales_tax->rate / 100), 'usd');
        }else{
            return new Money(0, 'usd');
        }
    }
    public function calculateTotal():Money{

        $subtotal = 0;

        $subtotal = $this->calculateSubTotal();
        $tax = $this->calculateTax();

        return new Money($subtotal->amount + $tax->amount, 'usd');

    }    

    public function toArray(){
        $lines = [];
        
        foreach($this->lines as $line){
            
            $lines[] = $line->toArray();
        }
        return [
            'id'=>$this->id,
            'number'=>$this->number,
            'status'=>$this->status->value,
            'company'=>$this->company?->toArray(),
            'billed_company'=>$this->billed_company->toArray(),

            'date'=>$this->date->format('Y-m-d H:i:s'),
            'date'=>$this->due_date->format('Y-m-d H:i:s'),
            'lines'=>$lines,
            'subtotal'=>$this->calculateSubTotal()->toArray(),
            'tax'=>$this->calculateTax()->toArray(),
            'total'=>$this->calculateTotal()->toArray(),

        ];
    

    }
    public function toJson(){
    
        return json_encode($this->toArray());
    }



}