<?php

namespace Tests\Unit;

use App\Modules\Invoices\Domain\ValueObjects\Money;
use DateTime;
use PHPUnit\Framework\TestCase;

class InvoicesTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->assertTrue(true);
    }

    public function test_MoneyValueObject()
    {

        $value = new \App\Modules\Invoices\Domain\ValueObjects\Money(999999999, 'usd' );
        $this->assertEquals("$9,999,999.99", (string)$value );
        
    }
    public function test_SalesTaxValueObject()
    {

        $tax = new \App\Modules\Invoices\Domain\ValueObjects\SalesTax(9.9 );
        $this->assertEquals(9.9, $tax->rate );
        $this->assertEquals("9.9%", (string)$tax);
        
    }
    public function test_CompanyEntity()
    {

        $company = new \App\Modules\Invoices\Domain\Entities\Company(null, name:"Company Inc", street:"647 Metropolitan way", city:"Des Plaines", zip:"60016",   phone: "+17730000000", email:null );
        $this->assertEquals("Company Inc", $company->name);
    }

    public function test_ProductEntity()
    {
        $product = new \App\Modules\Invoices\Domain\Entities\Product(null,  name: "Some product", price:new \App\Modules\Invoices\Domain\ValueObjects\Money(123456789, "USD") );
        
        $this->assertEquals(123456789, $product->price->amount);
        $this->assertEquals("Some product $1,234,567.89", (string)$product);
    }

    public function test_InvoiceAggregate()
    {
        
        
        $billed_company = new \App\Modules\Invoices\Domain\Entities\Company(null, name:"Billed Company Inc", street:"647 Metropolitan way", city:"Des Plaines", zip:"60016",  phone: "+17730000000", email:null );

        $product1 = new \App\Modules\Invoices\Domain\Entities\Product(null,  name: "Some product 1", price:new \App\Modules\Invoices\Domain\ValueObjects\Money(199, "USD") );
        
        $product2 = new \App\Modules\Invoices\Domain\Entities\Product(null,  name: "Some product 1", price:new \App\Modules\Invoices\Domain\ValueObjects\Money(299, "USD") );

        $invoice  = new \App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate(
            id:null,
            status:'draft',
            number: "1000",
            date: new \DateTime('now', new \DateTimeZone('UTC')),
            due_date: new \DateTime('2023-12-12', new \DateTimeZone('UTC')),
            company: null,
            billed_company: $billed_company,
            lines: [

                new \App\Modules\Invoices\Domain\Entities\InvoiceProductLine(null,  1, $product1),
                new \App\Modules\Invoices\Domain\Entities\InvoiceProductLine(null,  2, $product2),
            ],
            sales_tax: null,


        );
        
        $company = new \App\Modules\Invoices\Domain\Entities\Company(null, name:"Our Company Inc", street:"647 Metropolitan way", city:"Des Plaines", zip:"60016",  phone: "+17730000000", email:null );
        $invoice->setCompany($company);

        $invoice->applySalesTax(new \App\Modules\Invoices\Domain\ValueObjects\SalesTax(9.9 ));

        $totals_by_line = collect($invoice->toArray()['lines'])->sum(function($el){

            return $el['total']['amount'];
        });
       $this->assertEquals(797, $totals_by_line);
       $this->assertEquals(797, $invoice->toArray()['subtotal']['amount']);

       $this->assertEquals(79, $invoice->toArray()['tax']['amount']);

       $this->assertEquals(876, $invoice->toArray()['total']['amount']);
       $this->assertEquals("Our Company Inc", $invoice->toArray()['company']['name']);





    }





}
