<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


use App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class InvoicesControllerTest extends TestCase
{



    use RefreshDatabase;
    private $invoice_model;

    public function setUp():void{
        //
        parent::setUp();

        $this->seed(DatabaseSeeder::class);

        $this->invoice_model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::where('status', 'draft')->first();  
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->call('GET', '/api/test', ['ping'=>"pong"]);
        $this->assertEquals("pong", $response->json()['ping']);

        $response->assertStatus(200);
    }


    public function test_show()
    {

        $response = $this->call('GET', '/api/invoices/'.$this->invoice_model->id);
        $data = ($response->json());

        $this->assertEquals($this->invoice_model->id, $data['id']);

        $response->assertStatus(200);
        
        
  
    }

    public function test_show_exception()
    {

        $response = $this->call('GET', '/api/invoices/4f906f16-7d46-11ee-b962-0242ac120002');
        $data = ($response->json());
        $response->assertStatus(422);
        
    }


    public function test_approve()
    {

        $response = $this->put( '/api/invoices/'.$this->invoice_model->id.'/approve', []);
        $data = ($response->json());
        $this->assertEquals($this->invoice_model->id, $data['id']);

        $response->assertStatus(200);


        $response = $this->call('GET', '/api/invoices/'.$this->invoice_model->id);

        $data = ($response->json());
        $this->assertEquals('approved', $data['status']);
  
    }

    public function test_reject()
    {

        $response = $this->put( '/api/invoices/'.$this->invoice_model->id.'/reject', []);
        $data = ($response->json());
        $this->assertEquals($this->invoice_model->id, $data['id']);

        $response->assertStatus(200);


        $response = $this->call('GET', '/api/invoices/'.$this->invoice_model->id);

        $data = ($response->json());
        $this->assertEquals('rejected', $data['status']);
  
    }

    public function test_status_exception()
    {

        $this->invoice_model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::where('status', 'approved')->first();  
    
        $response = $this->put( '/api/invoices/'.$this->invoice_model->id.'/approve', []);
        $response->assertStatus(422);

        $response = $this->put( '/api/invoices/'.$this->invoice_model->id.'/reject', []);
        $response->assertStatus(422);


  
    }

   

}
