<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder;


class InvoicesInfrastructureTest extends TestCase
{
    use RefreshDatabase;

    private $invoice;
    public function setUp():void{
        //
        parent::setUp();

        $this->seed(DatabaseSeeder::class);


        $this->invoice = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::where('status', 'draft')->first();  

        
    }


    public function test_eloquent_models(){

        $invoices = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::all();      


        $this->assertTrue($invoices->count() > 0);

        $pp = \App\Modules\Invoices\Infrastructure\Database\Models\Product::all();      
        $this->assertTrue($pp->count() > 0);


        $model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::with('lines.product')->with('company')->find($invoices->first()->id);
           
        $this->assertTrue(isset ($model->toArray()['company']['id']));
        
        $this->assertEquals($invoices->first()->id, isset($model->toArray()['id']));

        $this->assertTrue(isset($model->toArray()['lines']));
        $this->assertTrue(count($model->toArray()['lines'])>0);
        $this->assertTrue(isset($model->toArray()['lines'][0]['product']['id']));


    }


    public function test_invoices_repository_getbyid()
    {

        $uuid = \Ramsey\Uuid\Uuid::fromString($this->invoice->id);
        $invoices_repository = new \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository();
        $entity = $invoices_repository->getById($uuid );

        $this->assertTrue( $entity instanceof \App\Modules\Invoices\Domain\Aggregates\InvoiceAggregate);

        $this->assertEquals($this->invoice->id, $entity->id);
        $this->assertEquals($uuid->toString(), $entity->id);
        $this->assertEquals(\App\Domain\Enums\StatusEnum::DRAFT,    $entity->status);
        $this->assertTrue(true);
    }

    public function test_invoices_repository_getbyid_exception()
    {

        $this->expectException(\App\Modules\Invoices\Application\InvoicesRepositoryException::class);
        $uuid = \Ramsey\Uuid\Uuid::uuid4();
        $invoices_repository = new \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository();
        $entity = $invoices_repository->getById($uuid );

    }


    public function test_invoices_repository_update()
    {

        $uuid = \Ramsey\Uuid\Uuid::fromString($this->invoice->id);
        $invoices_repository = new \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository();
        $entity = $invoices_repository->getById($uuid );
        $entity = $invoices_repository->setStatus($uuid, \App\Domain\Enums\StatusEnum::APPROVED );
        $invoice_model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::find($uuid->toString());
        $this->assertEquals(\App\Domain\Enums\StatusEnum::APPROVED->value,$invoice_model->status);

    }


    public function test_ApprovalAdapter_approve(){

        $uuid = \Ramsey\Uuid\Uuid::fromString($this->invoice->id);
        $invoices_repository = new \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository();
        $entity = $invoices_repository->getById($uuid );
        
        $adapter = app()->make(\App\Modules\Invoices\Application\ApprovalAdapterInterface::class);

        $adapter->approve($entity);
        $entity = $invoices_repository->getById($uuid );
        $this->assertEquals(\App\Domain\Enums\StatusEnum::APPROVED, $entity->status);
    }

    public function test_ApprovalAdapter_reject(){

        $uuid = \Ramsey\Uuid\Uuid::fromString($this->invoice->id);
        $invoices_repository = new \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository();
        $entity = $invoices_repository->getById($uuid );
        
        $adapter = app()->make(\App\Modules\Invoices\Application\ApprovalAdapterInterface::class);

        $adapter->reject($entity);
    
        $entity = $invoices_repository->getById($uuid );
        $this->assertEquals(\App\Domain\Enums\StatusEnum::REJECTED, $entity->status);

    }
    


}
