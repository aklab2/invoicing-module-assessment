<?php

namespace Tests\Feature;

use App\Modules\Invoices\Api\Dto\InvoiceDto;
use App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Mockery\MockInterface;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Event;
use LogicException;

class InvoicesApplicationTest extends TestCase
{

    use RefreshDatabase;
    private $invoice_model;

    public function setUp():void{
        //
        parent::setUp();

        $this->seed(DatabaseSeeder::class);

        $this->invoice_model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::where('status', 'draft')->first();  



    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_repository_di()
    {

        $invoices_repository = app()->make(\App\Modules\Invoices\Application\InvoicesRepositoryInterface::class);

        $this->assertTrue($invoices_repository instanceof \App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository);
        $this->assertTrue(true);
    }

    public function test_adapter_di()
    {

        $adapter = app()->make(\App\Modules\Invoices\Application\ApprovalAdapterInterface::class);

        $this->assertTrue($adapter instanceof \App\Modules\Invoices\Infrastructure\Adapters\ApprovalAdapter);
        $this->assertTrue(true);
    }


    public function test_service_getby_id()
    {
       
      

    
        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);

        $dto = $service->getInvoice($this->invoice_model->id);
            
        $this->assertTrue($dto instanceof InvoiceDto);
        $this->assertEquals($this->invoice_model->id, $dto->id->toString());

    }
    public function test_service_exception()
    {
        


        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Entity not found');


        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);
        //random UUID
        $dto = $service->getInvoice('4f906f16-7d46-11ee-b962-0242ac120002');
            


    }

    public function test_Service_approve()
    {
  

        Event::fake([

            \App\Modules\Approval\Api\Events\EntityApproved::class
        ]);



        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);

        $dto = $service->getInvoice($this->invoice_model->id);


        $service->approveInvoice($this->invoice_model->id);

        $entity = $service->getInvoice($this->invoice_model->id);

        $this->assertEquals(\App\Domain\Enums\StatusEnum::APPROVED, $entity->status);

  

    }   


    public function test_events_listener(){

        Event::fake([

            \App\Modules\Approval\Api\Events\EntityApproved::class
        ]);
    
        $dispatcher = app()->make(\Illuminate\Contracts\Events\Dispatcher::class);

        $uuid = \Ramsey\Uuid\Uuid::fromString($this->invoice_model->id);

        $dispatcher->dispatch(new \App\Modules\Approval\Api\Events\EntityApproved(new \App\Modules\Approval\Api\Dto\ApprovalDto($uuid, \App\Domain\Enums\StatusEnum::APPROVED, "")));

        Event::assertDispatched(\App\Modules\Approval\Api\Events\EntityApproved::class, function($event) use($uuid) {

            return $event->approvalDto->id->toString() == $uuid->toString();
      
        });


    }

    public function test_Service_reject()
    {
  
        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);

        $dto = $service->getInvoice($this->invoice_model->id);


        $service->rejectInvoice($this->invoice_model->id);

   
        $entity = $service->getInvoice($this->invoice_model->id);

        $this->assertEquals(\App\Domain\Enums\StatusEnum::REJECTED, $entity->status);


    }   


    public function test_Service_approve_exception()
    {
        
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('approval status is already assigned');

        $invoice_model = \App\Modules\Invoices\Infrastructure\Database\Models\Invoice::where('status', 'approved')->first();


        $service = app()->make(\App\Modules\Invoices\Api\InvoicesFacadeInterface::class);

        $service->approveInvoice($invoice_model->id);


    }      

}
