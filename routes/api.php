<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


use App\Modules\Invoices\Infrastructure\Controllers\InvoicesController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/test', [InvoicesController::class, 'test'] );

Route::get('/invoices/{id}', [InvoicesController::class, 'show'] );

Route::put('/invoices/{id}/approve', [InvoicesController::class, 'approve'] );
Route::put('/invoices/{id}/reject', [InvoicesController::class, 'reject'] );